import Controller from '@ember/controller';
import { action } from '@ember/object';

export default class ArtistNewController extends Controller {
  @action
  onSave(artist) {
    artist.save();
    this.transitionToRoute('artist.details', artist);
  }
}
