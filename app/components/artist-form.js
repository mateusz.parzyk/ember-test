import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ArtistFormComponent extends Component {
  @tracked artist;

  constructor(){
    super(...arguments);
    console.log('halo');
    console.log(this.args.artist);
    if (this.args.artist) {
      this.artist = this.args.artist;
    } 
  }

  @action
  submit(event) {
    event.preventDefault();
    this.args.onSave();
  }
}