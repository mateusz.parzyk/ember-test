import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class ArtistListElementComponent extends Component {
  @action
  remove() {
    if (this.args.onRemove) {
      this.args.onRemove();
    }
  }
}