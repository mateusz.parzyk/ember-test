import Component from '@glimmer/component';

export default class ArtistsListFilterComponent extends Component {
  get results() {
    let { artists, query } = this.args;

    if (query) {
      artists = artists.filter(artist => artist.name.includes(query));
    }

    return artists;
  }
}