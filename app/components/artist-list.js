import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ArtistListComponent extends Component {
  @tracked query = '';

  @action
  removeArtist(artist) {
    artist.destroyRecord();
  }
}
