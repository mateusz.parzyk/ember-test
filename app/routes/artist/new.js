import Route from '@ember/routing/route';

export default class ArtistNewRoute extends Route {
  async model() {
    this.newArtist = this.store.createRecord('artist');
    return this.newArtist
  }
  deactivate() {
    if (this.newArtist.isNew) {
      this.newArtist.rollbackAttributes();
    }
  }
}