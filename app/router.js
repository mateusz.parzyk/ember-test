import EmberRouter from '@ember/routing/router';
import config from 'artist-test/config/environment';

export default class Router extends EmberRouter {
  location = config.locationType;
  rootURL = config.rootURL;
}

Router.map(function() {
  this.route('index', {path: ''})
  this.route('artist', function() {
    this.route('list');
    this.route('details', {path: '/:artist_id'});
    this.route('new');
    this.route('edit',{path: '/:artist_id/edit'});

  });
});
