import { Factory } from 'ember-cli-mirage';

export default Factory.extend({
  name(i) {
    return `Artist ${i}`;
  },
  genere() {
    return 'POP'
  }
});
