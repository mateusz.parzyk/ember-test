export default function(server) {

  let artists = server.createList('artist', 10);
  artists.forEach(artist => {
    server.createList('song', 10, { artist });
  });
}
